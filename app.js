process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const express = require('express');
const app = express();
const path = require('path');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

// Keycloak
var Keycloak = require('keycloak-connect');
const session = require('express-session');

//Node-jose
const jose = require('node-jose');
const keystore =jose.JWK.createKeyStore();

//connect to MongoDB
mongoose.connect('mongodb://localhost/saga');
let db = mongoose.connection;
//chec connection (display a message if connected)
db.once('open', function(){
  console.log('Connected to db');
});

//Bring in Models
let User = require('./models/user');
let Token = require('./models/token')



var memoryStore = new session.MemoryStore();

app.use(session({
  secret: 'mySecret',
  resave: false,
  saveUninitialized: true,
  store: memoryStore
}));

var keycloak = new Keycloak({
  store: memoryStore
});

app.use(keycloak.middleware({
  logout: '/logout',
  admin: '/'
}));

//config view engine to view pug file
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

//set public folder containing: images, css, html ,.....
app.use(express.static(path.join(__dirname, 'public')));


app.get('/login-by-form', function(req, res){
  res.render('login-form');
});

app.post('/login-by-form', function(req, res){

});

app.get('*', function(req, res,next){
  res.locals.s = req.session['keycloak-token'];
  next();
});

app.get('/', function(req, res){
  res.render('layout');
});

app.get('/home', function(req, res){
  res.render('layout');
});

// const getUserProfile = https://www.googleapis.com/oauth2/v1/userinfo?access_token=xxx%27

app.get('/login', keycloak.protect(), function(req, res) {
  var obj = req.session['keycloak-token'];
  res.locals.token = req.token || null;
  var object = JSON.parse(req.session['keycloak-token']);
  // global.access = obj.access_token;
  console.log(object.id_token.payload );
  // const api_call = fetch(`https://www.googleapis.com/oauth2/v1/userinfo?access_token=${object.access_token}`);
  // const data = aoi_call.json();
  // console.log(data);


  // jwt.verify(req.session['keycloak-token'], process.env.JWT_SECRET, function(err, token){
  //   if(err) throw err;
  //   else{
  //       console.log(token)
  //   }
  // });

  res.redirect('/');
});

// app.use(keycloak.middleware( { logout: '/'} ));

app.listen(3000, function(){
  console.log('App is running on port 3000');
});
