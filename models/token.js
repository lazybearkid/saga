const mongoose = require('mongoose');

//Create user SAGA user schema

let tokenSchema = mongoose.Schema({
  access_token: {type: String, required: true},
  expires_in: {type: Number, required: true},
  refresh_expires_in: {type: Number, required: true},
  refresh_token: {type: String, required: true},
  token_type: {type: String, required: true},
  id_token: {type: String, required: true},
  not_before_policy: {type: Number, required: true},
  session_state: {type: String, required:true},
  scope: {type: String, required: true}
});

//export Model
module.exports = mongoose.model('Token', tokenSchema);
