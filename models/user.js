const mongoose = require('mongoose');

//Create user SAGA user schema

let userSchema = mongoose.Schema({
  email: {type: String, required: true},
  password: {type: String, required: true},
  name: {type: String, required: true}
});

//export Model
module.exports = mongoose.model('User', userSchema);
