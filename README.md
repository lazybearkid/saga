1.	Run: ```git clone https://lazybearkid@bitbucket.org/lazybearkid/saga.git```
---
	cd saga


2.	Install npm
---
	npm install

3.	Create ```package.json``` file
---
	npm init
Config main from index.js to app.js
	main: app.js

4.	Install mongodb driver
---
	npm install mongodb

5.	Download Mongodb
---
https://www.mongodb.com/download-center
For complete MongoDB installation instructions, see the manual.
Download the right MongoDB version from MongoDB
Create a database directory (in this case under /data).
Install and start a mongod process.
	
	mongod

6.	Install nodemon
---
	npm install nodemon


7.	Run application
---
	nodemon